package uz.pdp.demopcmarket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.demopcmarket.entity.template.AbsName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends AbsName {

    @Column(nullable = false)
    private String descreption;

    @Column(nullable = false)
    private String info;

    @OneToOne
    private Attachment attachment;

    @Column(nullable = false)
    private double price;

    @ManyToOne
    private Category category;
}
