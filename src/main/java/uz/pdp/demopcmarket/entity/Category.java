package uz.pdp.demopcmarket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.demopcmarket.entity.template.AbsName;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Category extends AbsName {

    private Integer parentCategoryId;

    @Column(nullable = false)
    private boolean active;
}
