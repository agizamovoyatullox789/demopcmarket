package uz.pdp.demopcmarket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.demopcmarket.entity.template.AbsName;

import javax.persistence.Column;
import javax.persistence.Entity;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class User extends AbsName {

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String email;
}
