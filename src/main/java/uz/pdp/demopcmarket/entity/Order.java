package uz.pdp.demopcmarket.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private Client client;

    @Column(nullable = false)
    private double totalPrice;

    @Column(nullable = false)
    private String orderNote;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false)
    private boolean delivred;
}
