package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.demopcmarket.entity.AttachmentContent;
import uz.pdp.demopcmarket.entity.Category;


public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
