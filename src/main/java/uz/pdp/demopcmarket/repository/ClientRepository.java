package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.demopcmarket.entity.Client;
import uz.pdp.demopcmarket.projection.ClientProjection;

@RepositoryRestResource(path = "client",collectionResourceRel = "list",excerptProjection = ClientProjection.class )
public interface ClientRepository extends JpaRepository<Client,Integer> {
}
