package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.demopcmarket.entity.Attachment;
import uz.pdp.demopcmarket.projection.AttachmentProjection;
import uz.pdp.demopcmarket.projection.UserProjection;

@RepositoryRestResource(path = "users",collectionResourceRel = "list",excerptProjection = UserProjection.class )
public interface UserRepository extends JpaRepository<Attachment,Integer> {
}
