package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.demopcmarket.entity.Category;
import uz.pdp.demopcmarket.entity.Order;
import uz.pdp.demopcmarket.entity.User;


public interface OrderRepository extends JpaRepository<Order,Integer> {
}
