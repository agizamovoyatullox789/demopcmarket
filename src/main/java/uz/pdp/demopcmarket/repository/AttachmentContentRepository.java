package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.demopcmarket.entity.AttachmentContent;


public interface AttachmentContentRepository extends JpaRepository<AttachmentContent,Integer> {
}
