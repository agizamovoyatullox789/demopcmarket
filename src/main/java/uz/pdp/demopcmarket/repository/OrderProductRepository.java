package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.demopcmarket.entity.Order;
import uz.pdp.demopcmarket.entity.OrderProducts;


public interface OrderProductRepository extends JpaRepository<OrderProducts,Integer> {
}
