package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.demopcmarket.entity.OrderProducts;
import uz.pdp.demopcmarket.entity.Product;


public interface ProductRepository extends JpaRepository<Product,Integer> {
}
