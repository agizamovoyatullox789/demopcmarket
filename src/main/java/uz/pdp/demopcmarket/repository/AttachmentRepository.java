package uz.pdp.demopcmarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.demopcmarket.entity.Attachment;
import uz.pdp.demopcmarket.projection.AttachmentProjection;

@RepositoryRestResource(path = "attachment",collectionResourceRel = "list",excerptProjection = AttachmentProjection.class )
public interface AttachmentRepository extends JpaRepository<Attachment,Integer> {
}
