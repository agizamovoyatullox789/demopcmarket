package uz.pdp.demopcmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.demopcmarket.entity.AttachmentContent;
import uz.pdp.demopcmarket.entity.Category;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.AttachmentContentDto;
import uz.pdp.demopcmarket.repository.AttachmentContentRepository;
import uz.pdp.demopcmarket.repository.AttachmentRepository;
import uz.pdp.demopcmarket.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    CategoryRepository categoryRepository;

    public List<Category> getCategorys() {
        return categoryRepository.findAll();
    }

    public Category getCategory(Integer id) {
        return categoryRepository.findById(id).orElseThrow(() -> new IllegalStateException("Category not found"));
    }

    public ApiResponse addCategory(Category category) {
       Category newCategory=new Category();
       newCategory.setName(category.getName());
       newCategory.setParentCategoryId(category.getParentCategoryId());
       newCategory.setActive(category.isActive());
       categoryRepository.save(newCategory);
        return new ApiResponse("Category added",true);
    }

    public ApiResponse editCategory(Integer id, Category category) {
        Optional<Category> byId = categoryRepository.findById(id);
        if (byId.isPresent()){
            Category newCategory = byId.get();
            newCategory.setName(category.getName());
            newCategory.setParentCategoryId(category.getParentCategoryId());
            newCategory.setActive(category.isActive());
            categoryRepository.save(newCategory);
            return new ApiResponse("Category edited",false) ;
        }
        return new ApiResponse("Category not found",false) ;
    }

    public ApiResponse deleteCategory(Integer id) {
        categoryRepository.deleteById(id);
        return new ApiResponse("Category deleted",true);
    }
}
