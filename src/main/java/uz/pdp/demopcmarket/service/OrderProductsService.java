package uz.pdp.demopcmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.demopcmarket.entity.Order;
import uz.pdp.demopcmarket.entity.OrderProducts;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.OrderDto;
import uz.pdp.demopcmarket.payload.OrderProductDto;
import uz.pdp.demopcmarket.repository.ClientRepository;
import uz.pdp.demopcmarket.repository.OrderProductRepository;
import uz.pdp.demopcmarket.repository.OrderRepository;
import uz.pdp.demopcmarket.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OrderProductsService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    OrderProductRepository orderProductRepository;
    @Autowired
    ProductRepository productRepository;

    public List<Order> getOrderProducts() {
        return orderRepository.findAll();
    }

    public Order getOrderProduct(Integer id) {
        return orderRepository.findById(id).orElseThrow(() -> new IllegalStateException("OrderProduct not found"));
    }

    public ApiResponse addOrderProduct(OrderProductDto orderProductDto) {
        OrderProducts orderProducts=new OrderProducts();
        orderProducts.setProduct(productRepository.getById(orderProductDto.getOrderId()));
        orderProducts.setAmount(orderProductDto.getAmount());
        orderProducts.setOrder(orderRepository.getById(orderProductDto.getProductId()));
        orderProductRepository.save(orderProducts);
        return new ApiResponse("OrderProduct added",true);
    }

    public ApiResponse editOrderProduct(Integer id,OrderProductDto orderProductDto) {
        Optional<OrderProducts> byId = orderProductRepository.findById(id);
        if (byId.isPresent()){
            OrderProducts orderProducts=byId.get();
            orderProducts.setProduct(productRepository.getById(orderProductDto.getOrderId()));
            orderProducts.setAmount(orderProductDto.getAmount());
            orderProducts.setOrder(orderRepository.getById(orderProductDto.getProductId()));
            orderProductRepository.save(orderProducts);
            return new ApiResponse("OrderProduct edited",false) ;
        }
        return new ApiResponse("OrderProduct not found",false) ;
    }

    public ApiResponse deleteOrderProduct(Integer id) {
        orderProductRepository.deleteById(id);
        return new ApiResponse("OrderProduct deleted",true);
    }
}
