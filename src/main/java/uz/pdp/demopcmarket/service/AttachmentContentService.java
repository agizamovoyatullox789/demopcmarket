package uz.pdp.demopcmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.demopcmarket.entity.AttachmentContent;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.AttachmentContentDto;
import uz.pdp.demopcmarket.repository.AttachmentContentRepository;
import uz.pdp.demopcmarket.repository.AttachmentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class AttachmentContentService {
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    public List<AttachmentContent> getAttachmentContents() {
        return attachmentContentRepository.findAll();
    }

    public AttachmentContent getAttachmentContent(Integer id) {
        return attachmentContentRepository.findById(id).orElseThrow(() -> new IllegalStateException("AttachmentContent not found"));
    }

    public ApiResponse addAttachmentContent(AttachmentContentDto attachmentContentDto) {
        AttachmentContent attachmentContent=new AttachmentContent(attachmentContentDto.getContent(),attachmentRepository.getById(attachmentContentDto.getAttachmentId()));
        attachmentContentRepository.save(attachmentContent);
        return new ApiResponse("AttachmentContent added",true);
    }

    public ApiResponse editAttachmentContent(Integer id, AttachmentContentDto attachmentContentDto) {
        Optional<AttachmentContent> optional = attachmentContentRepository.findById(id);
        if (optional.isPresent()){
            AttachmentContent attachmentContent = optional.get();
            attachmentContent.setContent(attachmentContent.getContent());
            attachmentContent.setAttachment(attachmentRepository.getById(attachmentContentDto.getAttachmentId()));
            return new ApiResponse("AttachmentContent edited",true) ;
        }
        return new ApiResponse("AttachmentContent not found",false) ;
    }

    public ApiResponse deleteAttachmentContent(Integer id) {
        attachmentContentRepository.deleteById(id);
        return new ApiResponse("AttachmentContent deleted",true);
    }
}
