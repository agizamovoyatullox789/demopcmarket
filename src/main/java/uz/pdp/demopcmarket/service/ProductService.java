package uz.pdp.demopcmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.demopcmarket.entity.Product;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.ProductDto;
import uz.pdp.demopcmarket.repository.*;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    CategoryRepository categoryRepository;

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public Product getProduct(Integer id) {
        return productRepository.findById(id).orElseThrow(() -> new IllegalStateException("Product not found"));
    }

    public ApiResponse addProduct(ProductDto productDto) {
        Product product=new Product();
        product.setName(productDto.getName());
        product.setDescreption(productDto.getDescription());
        product.setInfo(productDto.getInfo());
        product.setAttachment(attachmentRepository.getById(productDto.getAttachmentId()));
        product.setPrice(productDto.getPrice());
        product.setCategory(categoryRepository.getById(productDto.getCategoryId()));
        productRepository.save(product);
        return new ApiResponse("Product added",true);
    }

    public ApiResponse editProduct(Integer id, ProductDto productDto) {
        Optional<Product> byId = productRepository.findById(id);
        if (byId.isPresent()){
            Product product = byId.get();
            product.setName(productDto.getName());
            product.setDescreption(productDto.getDescription());
            product.setInfo(productDto.getInfo());
            product.setAttachment(attachmentRepository.getById(productDto.getAttachmentId()));
            product.setPrice(productDto.getPrice());
            product.setCategory(categoryRepository.getById(productDto.getCategoryId()));
            productRepository.save(product);
            return new ApiResponse("Product edited",false) ;
        }
        return new ApiResponse("Product not found",false) ;
    }

    public ApiResponse deleteProduct(Integer id) {
        productRepository.deleteById(id);
        return new ApiResponse("Product deleted",true);
    }
}
