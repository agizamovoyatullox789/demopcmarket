package uz.pdp.demopcmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.demopcmarket.entity.Category;
import uz.pdp.demopcmarket.entity.Order;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.OrderDto;
import uz.pdp.demopcmarket.repository.CategoryRepository;
import uz.pdp.demopcmarket.repository.ClientRepository;
import uz.pdp.demopcmarket.repository.OrderRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ClientRepository clientRepository;

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public Order getOrder(Integer id) {
        return orderRepository.findById(id).orElseThrow(() -> new IllegalStateException("Order not found"));
    }

    public ApiResponse addOrder(OrderDto orderDto) {
        Order order=new Order();
        order.setClient(clientRepository.getById(orderDto.getClientId()));
        order.setTotalPrice(orderDto.getTotalPrice());
        order.setOrderNote(orderDto.getOrderNote());
        order.setDate(orderDto.getDate());
        order.setDelivred(orderDto.isDelivred());
        orderRepository.save(order);
        return new ApiResponse("Order added",true);
    }

    public ApiResponse editOrder(Integer id,OrderDto orderDto) {
        Optional<Order> byId = orderRepository.findById(id);
        if (byId.isPresent()){
            Order order = byId.get();
            order.setClient(clientRepository.getById(orderDto.getClientId()));
            order.setTotalPrice(orderDto.getTotalPrice());
            order.setOrderNote(orderDto.getOrderNote());
            order.setDate(orderDto.getDate());
            order.setDelivred(orderDto.isDelivred());
            orderRepository.save(order);
            return new ApiResponse("Order edited",false) ;
        }
        return new ApiResponse("Order not found",false) ;
    }

    public ApiResponse deleteOrder(Integer id) {
        orderRepository.deleteById(id);
        return new ApiResponse("Order deleted",true);
    }
}
