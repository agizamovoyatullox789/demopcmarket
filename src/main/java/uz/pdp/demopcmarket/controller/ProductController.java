package uz.pdp.demopcmarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.demopcmarket.entity.Order;
import uz.pdp.demopcmarket.entity.Product;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.OrderDto;
import uz.pdp.demopcmarket.payload.ProductDto;
import uz.pdp.demopcmarket.service.OrderService;
import uz.pdp.demopcmarket.service.ProductService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/orders")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping
    public HttpEntity<List<Product>> getProducts(){
        List<Product> products = productService.getProducts();
        return ResponseEntity.ok(products);
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable Integer id){
        return productService.getProduct(id);
    }
    @PostMapping
    public HttpEntity<ApiResponse> addProduct(@RequestBody ProductDto productDto){
        ApiResponse apiResponse=productService.addProduct(productDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editProduct(@PathVariable Integer id,@RequestBody ProductDto productDto){
        ApiResponse apiResponse=productService.editProduct(id,productDto);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> deleteProduct(@PathVariable Integer id){
        ApiResponse apiResponse=productService.deleteProduct(id);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String,String> handleValidationExceptions(
            MethodArgumentNotValidException ex){
        Map<String ,String> errors=new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage=error.getDefaultMessage();
            errors.put(fieldName,errorMassage);
        });
        return errors;
    }
}
