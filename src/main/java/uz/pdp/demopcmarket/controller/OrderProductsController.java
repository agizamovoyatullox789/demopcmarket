package uz.pdp.demopcmarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.demopcmarket.entity.Order;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.OrderDto;
import uz.pdp.demopcmarket.service.OrderService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/orders")
public class OrderProductsController {
    @Autowired
    OrderService orderService;

    @GetMapping
    public HttpEntity<List<Order>> getOrders(){
        List<Order> categoryList = orderService.getOrders();
        return ResponseEntity.ok(categoryList);
    }

    @GetMapping("/{id}")
    public Order getOrder(@PathVariable Integer id){
        return orderService.getOrder(id);
    }
    @PostMapping
    public HttpEntity<ApiResponse> addOrder(@RequestBody OrderDto orderDto){
        ApiResponse apiResponse=orderService.addOrder(orderDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editOrder(@PathVariable Integer id,@RequestBody OrderDto orderDto){
        ApiResponse apiResponse=orderService.editOrder(id,orderDto);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> deleteOrder(@PathVariable Integer id){
        ApiResponse apiResponse=orderService.deleteOrder(id);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String,String> handleValidationExceptions(
            MethodArgumentNotValidException ex){
        Map<String ,String> errors=new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage=error.getDefaultMessage();
            errors.put(fieldName,errorMassage);
        });
        return errors;
    }
}
