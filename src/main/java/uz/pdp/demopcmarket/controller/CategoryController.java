package uz.pdp.demopcmarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.demopcmarket.entity.AttachmentContent;
import uz.pdp.demopcmarket.entity.Category;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.AttachmentContentDto;
import uz.pdp.demopcmarket.service.AttachmentContentService;
import uz.pdp.demopcmarket.service.CategoryService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping
    public HttpEntity<List<Category>> getCategorys(){
        List<Category> categoryList = categoryService.getCategorys();
        return ResponseEntity.ok(categoryList);
    }

    @GetMapping("/{id}")
    public Category getCategory(@PathVariable Integer id){
        return categoryService.getCategory(id);
    }
    @PostMapping
    public HttpEntity<ApiResponse> addCategory(@RequestBody Category category){
        ApiResponse apiResponse=categoryService.addCategory(category);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editCategory(@PathVariable Integer id,@RequestBody Category category){
        ApiResponse apiResponse=categoryService.editCategory(id,category);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> deleteCategory(@PathVariable Integer id){
        ApiResponse apiResponse=categoryService.deleteCategory(id);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String,String> handleValidationExceptions(
            MethodArgumentNotValidException ex){
        Map<String ,String> errors=new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage=error.getDefaultMessage();
            errors.put(fieldName,errorMassage);
        });
        return errors;
    }
}
