package uz.pdp.demopcmarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.demopcmarket.entity.AttachmentContent;
import uz.pdp.demopcmarket.payload.ApiResponse;
import uz.pdp.demopcmarket.payload.AttachmentContentDto;
import uz.pdp.demopcmarket.service.AttachmentContentService;

import javax.annotation.security.PermitAll;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/attachment")
public class AttachmentContentController {
    @Autowired
    AttachmentContentService attachmentContentService;

    @GetMapping
    public HttpEntity<List<AttachmentContent>> getAttachmentContents(){
        List<AttachmentContent> attachmentContents = attachmentContentService.getAttachmentContents();
        return ResponseEntity.ok(attachmentContents);
    }

    @GetMapping("/{id}")
    public AttachmentContent getAttachmentContent(@PathVariable Integer id){
        return attachmentContentService.getAttachmentContent(id);
    }
    @PostMapping
    public HttpEntity<ApiResponse> addAttachmentContent(@RequestBody AttachmentContentDto attachmentContentDto){
        ApiResponse apiResponse=attachmentContentService.addAttachmentContent(attachmentContentDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> editAttachmentContent(@PathVariable Integer id,@RequestBody AttachmentContentDto attachmentContentDto){
        ApiResponse apiResponse=attachmentContentService.editAttachmentContent(id,attachmentContentDto);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> deleteAttachmentContent(@PathVariable Integer id){
        ApiResponse apiResponse=attachmentContentService.deleteAttachmentContent(id);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String,String> handleValidationExceptions(
            MethodArgumentNotValidException ex){
        Map<String ,String> errors=new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage=error.getDefaultMessage();
            errors.put(fieldName,errorMassage);
        });
        return errors;
    }
}
