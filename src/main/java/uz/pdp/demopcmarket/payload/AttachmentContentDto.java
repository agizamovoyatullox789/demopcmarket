package uz.pdp.demopcmarket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttachmentContentDto {
    private byte[] content;
    private Integer attachmentId;

}
