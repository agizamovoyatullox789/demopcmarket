package uz.pdp.demopcmarket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private Integer clientId;
    private double totalPrice;
    private String orderNote;
    private Date date;
    private boolean delivred;
}
