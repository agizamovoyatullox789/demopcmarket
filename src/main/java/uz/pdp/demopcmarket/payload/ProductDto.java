package uz.pdp.demopcmarket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private String name;
    private String description;
    private String info;
    private Integer attachmentId;
    private double price;
    private Integer categoryId;
}
