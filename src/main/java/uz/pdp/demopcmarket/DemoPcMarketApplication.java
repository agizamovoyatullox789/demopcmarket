package uz.pdp.demopcmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPcMarketApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoPcMarketApplication.class, args);
    }

}
