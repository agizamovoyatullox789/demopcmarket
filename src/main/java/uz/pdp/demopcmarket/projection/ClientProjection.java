package uz.pdp.demopcmarket.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.demopcmarket.entity.Client;

@Projection(types = Client.class)
public interface ClientProjection {
    Integer getId();
    String getName();
    String getAddress();
    String getPhoneNumber();
    String getEmail();
}
