package uz.pdp.demopcmarket.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.demopcmarket.entity.User;

@Projection(types = User.class)
public interface UserProjection {
    String getName();
    String getPassword();
    String getLogin();
    String getEmail();
}
