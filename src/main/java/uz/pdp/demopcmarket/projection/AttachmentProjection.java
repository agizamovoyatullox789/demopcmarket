package uz.pdp.demopcmarket.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.demopcmarket.entity.Attachment;

@Projection(types = Attachment.class)
public interface AttachmentProjection {
    Integer getId();
    String getName();
    String getContentType();
    double getSize();
}
